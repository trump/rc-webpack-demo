import React from 'react';

var layoutLess = require('!style/useable!css!less!./Layout.less');

export default class Layout extends React.Component {
    constructor(props) {
        super(props);
        this.props = props;
        this.state = this.getInitialState();
    }

    getInitialState() {
        return {};
    }

    applyStyle() {
        layoutLess.use();
    }

    removeStyle() {
        layoutLess.unuse();
    }

    render() {
        return <div>
            <h1>Layout </h1>

            <ul>
                <li> <a href="javascript:;" onClick={this.applyStyle.bind(this)}> applyStyel </a> </li>
                <li> <a href="javascript:;" onClick={this.removeStyle.bind(this)}> removeStyle </a> </li>
            </ul>
            {this.props.children}
        </div>;
    }
}