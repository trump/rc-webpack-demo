require.ensure([
    'react'
    , './Layout.jsx'
], require=> {

    var React = require('react');
    var Layout = require('./Layout.jsx');
    var Image = require('./Image.jsx');
    var $ = require('jquery');

    var pageContent = <Layout>

        <Image/>

        Page 2
        <br/>
    </Layout>;

    $(()=> React.render(pageContent, document.body));
});

