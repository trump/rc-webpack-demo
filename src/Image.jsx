var React = require('react');

var img = require('./images/what-is-webpack.png');

module.exports = React.createClass({
    displayName: 'Image'
    , render() {
        return <img width="300px" src={img} alt={img}/>
    }
});