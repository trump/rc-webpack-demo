/**
 * Created by trump on 15/5/21.
 */
var path = require('path');
var CommonsChunkPlugin = require('webpack/lib/optimize/CommonsChunkPlugin');
var UglifyJsPlugin = require('webpack/lib/optimize/UglifyJsPlugin');


var isProd = process.argv.indexOf('--mode-prod') >= 0; // check prod mode

var plugins = [
    new CommonsChunkPlugin("application.commons.js", ["page1", "page2", "page3"])
];

if (isProd) {
    plugins.push(new UglifyJsPlugin({minimize: true}));
}

module.exports = {
    entry: {
        page1: './page1.js'
        , page2: './page2.js'
        , page3: './page3.js'
    },
    plugins: plugins,
    output: {
        filename: '[name].js' //this is the default name, so you can skip it

        //at this directory our bundle file will be available
        //make sure port 8090 is used when launching webpack-dev-server
        , publicPath: '/build/'

        , path: path.join(__dirname, "../build")
    },

    module: {
        loaders: [
            {
                //tell webpack to use jsx-loader for all *.jsx files
                test: /\.jsx?$/,
                exclude: /(node_modules|bower_components)/,
                loader: 'babel'
            }

            , {
                test: /\.(jpe?g|png|gif|svg)$/i,
                loaders: [
                    'file?hash=sha512&digest=hex&name=[hash].[ext]',
                    'image-webpack?bypassOnDebug&optimizationLevel=7&interlaced=false'
                ]
            }

            , {test: /\.css$/, loader: "style!css?minimize"}

            , {
                test: /\.less$/,
                loader: "style!css?minimize!less"
            }

            // font loaders
            , {
                test: /\.woff(2)?(\?v=[0-9]\.[0-9]\.[0-9])?$/,
                loader: "url-loader?limit=10000&minetype=application/font-woff"
            }

            , {test: /\.(ttf|eot|svg)(\?v=[0-9]\.[0-9]\.[0-9])?$/, loader: "file-loader"}

            , {
                //tell webpack to use jsx-loader for all *.jsx files
                test: /\.json$/,
                loader: 'json-loader'
            }

            , {test: /hello/, loader: 'helloworld-loader'}
        ]
    },
    externals: {
        //don't bundle the 'react' npm package with our bundle.js
        //but get it from a global 'React' variable
        'jquery': 'jQuery'
    },
    resolve: {
        extensions: ['', '.js', '.jsx']
    }
};

