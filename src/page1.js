import React from 'react';
import Layout from './Layout.jsx';
import Image from './Image.jsx';
import $ from 'jquery';

var helloWorldText = require('./hello');

var pageContent = <Layout>

    <Image/>

    Page 1
    <br/>
    {helloWorldText}
</Layout>;

$(()=> React.render(pageContent, document.body));